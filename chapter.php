<?php
  header("X-Frame-Options: DENY");

  require_once 'php/db.php';
  require_once 'php/functions.php';

  //撈取全部標題
  $title = get_all_title();

  //撈取目標章節內容
  $dates1 = get_article_content($_GET['cha']);
  
  $get_id = $dates1['id'];

  $Previous = null;
  $Next = null;

  //print_r($dates1);

  //使用迴圈處理上一頁
  for ($i = count($title) -1; $i >= 0; $i--)
  {
    if ($i - 1 != -1)
    {
      if ($title[$i]['id'] == $get_id)
      {
        $Previous = $title[$i - 1]['id'];
        break;
      }
    }
    else
    {
      if ($title[$i]['id'] == $get_id)
      {
        $Previous = $title[$i]['id'];
        break;
      }
    }
  }

  //使用迴圈處理下一頁
  for($i = 0; $i < count($title); $i++)
  {
    if ($i != count($title) - 1)
    {
      if ($title[$i]['id'] == $get_id)
      {
        $Next = $title[$i + 1]['id'];
        break;
      }
    }
    else
    {
      if ($title[$i]['id'] == $get_id)
      {
        $Next = $title[$i]['id'];
        break;
      }
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/cs_chapter.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--top-->
    <div id="top">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card text_center">
            <a class="CShool" href="index.php">C# School
              <span class="com">.com</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <?php include_once 'menu.php';?>

    <!-- 教程目錄 -->
    <div id="Tutorial">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card align_right">
            <span class="close">
              <i class="fas fa-times"></i>
            </span>
          </div>
        </div>

        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card">
            <h2 class="title">C# 教程</h2>
          </div>
        </div>

        <?php if (!empty($title)):?>
          <?php foreach($title as $key=>$row):?>
            <div class="cs-col sm12 md12 lg12 xl12">
              <div class="card">
                <?php if ($row['title'] == $dates1['title']):?>
                  <a class="list" href="./chapter.php?cha=<?php echo $row['id']?>" target="_top" style="background-color: #00bfff; color: #ffffff"><?php echo $row['title'];?></a>
                <?php else:?>
                  <a class="list" href="./chapter.php?cha=<?php echo $row['id']?>" target="_top"><?php echo $row['title'];?></a>
                <?php endif;?>
              </div>
            </div>
          <?php endforeach;?>
        <?php endif;?>
      </div><!--row-->
    </div><!--Tutorial-->

    <!-- 教程內容 -->
    <div id="Tutorial_content">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <?php if (!empty($dates1['title'])):?>
                <h1 class="title"><?php echo $dates1['title'];?></h1>
              <?php else:?>
                <h1 class="title">無標題</h1>
              <?php endif;?>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <a class="previous" href="./chapter.php?cha=<?php echo $Previous;?>">< 上一頁</a>
              <a class="next align_right" href="./chapter.php?cha=<?php echo $Next;?>">下一頁 ></a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <?php if (!empty($dates1['content'])):?>
                <p><?php echo $dates1['content'];?></p>
              <?php else:?>
                <p>無內容</p>
              <?php endif;?>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <a class="previous" href="./chapter.php?cha=<?php echo $Previous;?>">< 上一頁</a>
              <a class="next" href="./chapter.php?cha=<?php echo $Next;?>">下一頁 ></a>
            </div>
          </div>
        </div>

        <!--聯絡方式-->
        <div id="contact">
          <div class="container">
            <div class="row">
              <div class="cs-col sm12 md12 lg12 xl12">
                <div class="card">
                  <h2 class="title">聯繫我們</h2>
                </div>
              </div>

              <div class="cs-col sm12 md12 lg12 xl12">
                <div class="card">
                  <!--連結google社群-->
                  <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
                    <img class="logo_g" src="image/box-google1.png">
                  </a>

                  <!--連結facebook社群-->
                  <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
                    <img class="logo_f" src="image/box-facebook1.png">
                  </a>

                  <!--連結line社群-->
                  <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
                    <img class="logo_l" src="image/box-line1.png">
                  </a>
                </div>
              </div>

              <div class="cs-col sm12 md12 lg12 xl12">
                <div class="card">
                  <p class="copyright">Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
                </div>
              </div>
            </div><!--row-->
          </div><!--container-->
        </div><!--contact-->
    </div><!--Tutorial_content-->
  </body>
</html>

<script>
  $(document).on("ready", function(){
    $(window).on("resize", function(){
      var w = $(window).width();      

      if (w > 768)
      {
        $("#menu ul.level1 li.list").removeAttr('style');
        $("#menu ul.level1 li.main").removeAttr('style');
        $("#Tutorial").removeAttr('style');
      }
    });

    $("#menu ul.level1 li.more").on("click", function(){
      $("#menu ul.level1 li.list").toggle();
      $("#menu ul.level1 li.main").toggle();
    });

    $("#menu ul.level1 li.logo").on("click", function(){
      $("#Tutorial").toggle();
    });

    $("#Tutorial .cs-col span.close").on("click", function(){
      $("#Tutorial").toggle();
    });

    $(window).on("scroll", function(){
      var up = $(window).scrollTop();
      //console.log(up);
      
      if (up > 90)
      {
        $("#Tutorial").css({"top": "52px"});
      }
      else
      {
        $("#Tutorial").css({"top": "136px"});
      }
    });
  })
</script>