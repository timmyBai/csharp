﻿<?php
  header("X-Frame-Options: DENY");
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/cs_index.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--top-->
    <div id="top">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card text_center">
            <a class="CShool" href="index.php">C# School
              <span class="com">.com</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <?php include_once 'menu.php'?>

    <!-- 歡迎介面 -->
    <div id="welcome">      
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 x12">
            <h1 class="title">Visual Studio 2017 C#</h1>
          </div>

          <div class="cs-col sm12 md12 lg12 x12">
            <p class="content">
              隨著電腦硬體技術不斷地進步以及軟體的日新月異，指揮電腦運作的程式語言，由早期的機器語言、低階語言、結
              構化高階語言:FORTRAN、COBOL、BASIC、C、PHP，演進到Java、C++、和 C#，其中BASIC和C程式語言已成為目
              前初學者學習程式語言的兩大主流。
            </p>
          </div>

          <div class="cs-col sm12 md12 lg12 x12">
            <p class="content">
              微軟的Visual Studio提供一個編輯工具、偵錯和分析工具，以及具編譯和執行能力的整合開發環境(IDE)，也提
              供C#、C++、JavaScript、Python、Visual Basic等多種程式語言，讓使用者可以在同一個IDE下，設計出可以
              在Windows、Android、iOS等多種平台是執行
            </p>
          </div>
          
          <div class="cs-col sm12 md12 lg12 x12">
            <p class="content">
              本網站主要解紹 C# 程式語言，讓使用者在剛接觸程式語言的時候，可以先熟悉程式基本的流程、以及程式設計的
              基本觀念，透過多種程式範例，培養使用者在程式設計上有個基本的素養和邏輯思考能力，希望透過這個教學網站
              可以培養出更多寫程式的人才。
            </p>
          </div>
        </div>
      </div>
    </div>

    <!--Visual Studio 2017 功能介紹-->
    <div id="Introduction">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <h1 class="title">Visual Studio 2017 功能介紹</h1>
        </div>

        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="youtubeRWD">
            <iframe width="560px" height="315px" src="https://www.youtube.com/embed/TYEoJorFn54" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>

    <!--教學特色-->
    <div id="Features">      
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <h1 class="title">本網站教學特色</h1>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <hr>
          </div>

          <div class="cs-col sm12 md6 lg4 xl4">
            <div class="card">
              <span>
                <i class="fas fa-code"></i>
              </span>
              <p class="content">本團隊提供多種程式碼的範例，讓學者們能多多熟悉這些程式碼的應用，以及懂得程式碼的跑法。</p>
            </div>
          </div>

          <div class="cs-col sm12 md6 lg4 xl4">
            <div class="card">
              <span>
                <i class="fas fa-cogs"></i>
              </span>
              <p class="content">本團隊帶領剛接觸程式設計的學員們，培養寫成式的邏輯思考能力，及程式設計能力。</p>
            </div>
          </div>

          <div class="cs-col sm12 md6 lg4 xl4">
            <div class="card">
              <span>
                <i class="fas fa-user-graduate"></i>
              </span>
              <p class="content">本團隊希望每個人進來學習程式語言的學員們，可以有個基本的程式素養。</p>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div><!--Features-->

    <!--聯絡我們-->
    <div id="contact">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <h2 class="title">聯繫我們</h2>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <!--連結google社群-->
              <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
                <img class="logo_g" src="image/box-google1.png">
              </a>

              <!--連結facebook社群-->
              <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
                <img class="logo_f" src="image/box-facebook1.png">
              </a>

              <!--連結line社群-->
              <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
                <img class="logo_l" src="image/box-line1.png">
              </a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <p class="copyright">Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div><!--contact-->
  </body>
</html>

<script>
  $(document).on("ready", function(){
    $(window).on("resize", function(){
      var w = $(window).width();

      if (w > 768)
      {
        $("#menu ul.level1 li.list").removeAttr('style');
        $("#menu ul.level1 li.main").removeAttr('style');
      }
    })

    $("#menu ul.level1 li.logo").on("click", function(){
        $("#menu ul.level1 li.list").toggle();
        $("#menu ul.level1 li.main").toggle();
        $("#menu ul.level1 li.list").css({"z-index": "999"});
    })
  })
</script>