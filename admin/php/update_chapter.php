<?php
    require_once 'db.php';
    require_once 'functions.php';

    if (isset($_SESSION['link']) && $_SESSION['link'])
    {
        $update_chapter = update_chapter($_POST['id'], $_POST['title'], $_POST['content'], $_POST['publish'], $_POST['username']);

        if ($update_chapter)
        {
            echo 'yes';
        }
        else
        {
            echo 'no';
        }
    }
    else
    {
        echo 'no';
    }
?>