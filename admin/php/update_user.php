<?php
  require_once 'db.php';
  require_once 'functions.php';
  
  if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    $update_user = update_user($_POST['user_id'], $_POST['username'], $_POST['account'], $_POST['identity'], $_POST['state'], $_POST['username']);

    if ($update_user)
    {
      echo 'yes';
    }
    else
    {
      echo 'no';
    }
  }
  else
  {
    echo 'no';
  }
?>
