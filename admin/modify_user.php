<?php
  require_once 'php/db.php';

  if (!isset($_SESSION['is_login']) && !$_SESSION['is_login'])
  {
    header("Location: login.php");
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School 後台登入</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/modify_pw.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/modify_user_pass.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="container1">
      <div class="content">
        <div id="login" class="box">
          <div class="logo">
            <span class="csharp"><b>C# School</b></span>
            <span class="com"><b>.com</b></span>
            <span>會員修改密碼</span>
          </div>

          <div id="level1">
            <div id="level2">
              <div class="contents">
                <h3 class="username"><b>帳號/Account: <b></h3>
                <input id="box-account" class="box_account" type="text" value="<?php echo $_SESSION['is_account']?>" placeholder="請輸入帳號 Account">
                  <p id="ex1" class="error"></p>
              </div>
              
              <div class="contents">
                <h3 class="oldPassword"><b>輸入舊密碼/Old Password:</b></h3>
                <input id="box-oldpassword" class="box_oldpassword" type="password" name="password" maxlength="20" placeholder="輸入舊密碼 Old Password">
                <p id="ex2" class="error"></p>
              </div>

              <div class="contents">
                <h3 class="newPassword"><b>輸入新密碼/New Password:</b></h3>
                <input id="box-newpassword" class="box_newpassword" type="password" name="password" maxlength="20" placeholder="輸入新密碼 New Password">
                <p id="ex3" class="error"></p>
              </div>

              <div class="contents">
                <h3 class="confirmPassword"><b>輸入確認新密碼/Confirm Passwd:</b></h3>
                <input id="box-confirmpassword" class="box_confirmpassword" type="password" name="password" maxlength="20" placeholder="輸入確認新密碼 Confirm Passwd">
                <p id="ex4" class="error"></p>
              </div>

              <div class="contents">
                <button id="box-submit" class="confirm" type="submit"><b>確定</b></button>
                <button class= "leave" type="button"><a href="index.php"><b>離開</b></a></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>