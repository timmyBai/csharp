<?php
  require_once 'php/db.php';

  if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    header("Location: index.php");
  }

  /*if ($_SERVER['REMOTE_ADDR'] == "120.107.132.205" || $_SERVER['REMOTE_ADDR'] == "120.107.132.206" || $_SERVER["REMOTE_ADDR"] == "120.107.97.154")
  {
    require_once '../php/db.php';

    if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
    {
      header("Location: index.php");
    }
  }
  else
  {
    header("Location: ../");
    exit();
  }*/
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School 後台登入</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/login.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/login.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="container">
      <div id="content">
        <div id="login" class="box">
          <div class="logo">
            <span class="csharp"><b>C# School</b></span>
            <span class="com"><b>.com</b></span>
            <span><b>後台登入</b></span>
          </div>

          <div id="level1">
            <div id="level2">
              <form id="frm1" class="login">
                <div class="contents">
                  <h3 class="user"><b>帳號/Account:</b></h3>
                  <input id="box-account" class="box-input" type="text" name="Email" maxlength="40" placeholder="輸入帳號 account">
                  <p id="ex1"></p>
                </div>

                <div class="contents">
                  <h3 class="pass"><b>密碼/PassWord:</b></h3>
                  <input id="box-password" class="box-input" type="password" name="password" maxlength="20" placeholder="輸入密碼 password">
                  <p id="ex2"></p>
                </div>

                <div class="contents">
                  <button id="box-submit" class="submit" type="submit" name="button"><b>登入</b></button>
                </div>
              </form>
            </div><!--level2 Exit-->
          </div><!--level1 Exit-->
        </div><!--box login Exit-->
      </div><!--box-content Exit-->
    </div>

    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>
