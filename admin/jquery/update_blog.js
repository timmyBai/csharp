$(document).on('ready', function(){
    let count = 0;

    function checkField(title, publish)
    {
        let arr = [title, publish];
        $.each(arr, function(index, value){
            let self = value.val();
            if (self === '' || typeof(self) === undefined)
            {
                count = count + 1;
            }
        });
    }

    $('button#save-chapter').on('click', function(){
        const id = $('input[name=article_blog_id]');
        const title = $('input[name=title]');
        const content = CKEDITOR.instances.editor1.getData(); //ckeditor api 取得文本內容
        const publish = $('input[name=publish]:checked');
        const username = $('span.username');

        checkField(title, publish);

        if (count == 0)
        {
            $.ajax({
                type: 'POST',
                url: 'php/update_blog.php',
                data: {
                    id: id.val(),
                    title: title.val(),
                    content: content,
                    publish: publish.val(),
                    username: username.text()
                },
                dataType: 'html'
            }).done(function(data){
                if (data == 'yes')
                {
                    alert("更新成功，請查看列表確認。");
                    location.assign(location);
                }
                else
                {
                    alert("修改失敗");
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                alert('有錯誤產生，請看 console.log');
                console.log(jqXHR.responeText);
            });
        }
        else
        {
            count = 0;
            alert("所有欄位皆須填寫");
        }

        return false;
    });
})