$(document).on("ready", function(){
  $("#box-account").on("input",function(){
    this.value = this.value.replace(/[^0-9A-Za-z]/g,'');
  });

  $("#box-password").on("input", function(){
    this.value = this.value.replace(/[^0-9a-zA-Z]/g,'');
  });

  //按下 登入 按鈕後
  $("#box-submit").on("click", function(){
    var fac = false, fpw = false;

    if ($("#box-account").val() == '') //確認 account 有無輸入
    {
      $("#ex1").text("未輸入帳號").css({"color": "#ff0000"});
    }
    else
    {
      fac = true;
    }

    if($("#box-password").val() == '') //確認 password 有無輸入
    {
      $("#ex2").text("未輸入密碼").css({"color": "#ff0000"});
    }
    else
    {
      fpw = true;
    }

    if (fac == true && fpw == true) //start verify <input> YesNo blank
    {
      //若以上條件都正確，則使用 ajax 傳送資料
      $.ajax({
        type: "POST", //使用 POST 方式傳送資料
        url: "php/verify_user.php",
        data: {
          ac: $("#box-account").val(),
          pw: $("#box-password").val()
        },
        dataType: 'html'
      }).done(function(data){
        //傳送成功的時候
        console.log(data);
        if (data == "yes")
        {
          window.location.href = "index.php";
        }
        else
        {
          $("#box-password").val("");
          $("#ex2").text("登入失敗，請確認帳號密碼是否輸入正確").css({"color": "#ff0000"});
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //傳送失敗的時候
        alert("有錯誤產稱請看 consle.log");
        console.log(jqXHR.resoneText);
      });
    }//Exit verify input blank

    return false;

  })//submit Exit

});
