$(document).on("ready", function(){
  //限制 username 不可以輸入特殊符號
  $("#username").on('input', function(){
    this.value = this.value.replace(/[\\\.\/\*\-\+\~\`\!\_\@\#\$\%\^\&\(\)\{\}\[\]\:\;\"\'\<\>\,\=]/g, "");
  });

  //限制 account 只能輸入英文、數字
  $("#account").on('input', function(){
    this.value = this.value.replace(/[^A-Za-z0-9]/g, "");
  });

  //限制 passwd 只能輸入英文、數字
  $("#password").on('input', function(){
    this.value = this.value.replace(/[^A-Za-z0-9]/g, "");
  });

  //檢查名稱是否重複
  $("#username").on("keyup", function(){
    //console.log($(this).val()); //確定是否取得 username 的值
    if ($(this).val() != '')
    {
      $.ajax({//使用 ajax 傳送資料
        type: "POST", //表單傳送的方式同 form 的 mothed
        url: "php/check_username.php", //目標給哪的檔案，同 form 的 action 屬性
        data:{ /*為要傳過去的資料，使用物件方式呈現，引為變數 key 值為英文的關係，所以用物件方式傳送。
                ex: {name: "輸入", password: "輸入密碼"}*/
          n : $(this).val() //代表要傳的一個 n 變數值為，username 文字方塊
        },
        dataType: 'html' //設定該回應的會是 html 格式
      }).done(function(data){
        //console.log(data);  //確認資料傳送後是否得到資料回應
        //成功的時候回傳資料
        if (data == 'yes')
        {
          //暱稱已存在，把文字方塊變成紅色
          $("#username").css({"border": "1px solid #ff0000"});
          $("#ex1").text("此暱稱已存在，不可新增").css({"color": "#ff0000"});
          $("#save-user").attr('disabled', true).css({"background-color": "#80dfff"});
        }
        else
        {
          $("#username").css({"border": "1px solid #00bfff"});
          $("#ex1").text("");
          $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //失敗的時候
        alert('有錯誤產生，請看 console.log');
        console.log(jqXHR.responeText);
      });
    }
    else
    {
      //不檢查暱稱是否重複
      //如果 input 為空值得話把文字方塊變回藍色
      $("#username").css({"border": "1px solid #00bfff"});
      $("#ex1").text("");
      $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
    }
  });//結束檢查


  //檢查 account 帳號是否重複
  $("#account").on("keyup", function(){
    //console.log($(this).val()); //確定是否取得 Email 的值
    //檢查 Email 有沒有重複
    if ($(this).val() != '')
    {
      $.ajax({
        type: "POST",
        url: "php/check_account.php",
        data:{
          e: $(this).val()
        },
        dataType: 'html'
      }).done(function(data){
        //傳送成功的時候
        if (data == 'yes')
        {
          //account 已存在，把文字方塊變紅色
          $("#account").css({"border": "1px solid #ff0000"});
          $("#ex2").text("此帳號已註冊，請使用別的帳號").css({"color": "#ff0000"});
          $("#save-user").attr('disabled', true).css({"background-color": "#80dfff"});
        }
        else
        {
          $("#account").css({"border": "1px solid #00bfff"});
          $("#ex2").text("");
          $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //傳送失敗的時候
        alert("有錯誤產稱請看 consle.log");
        console.log(jqXHR.resoneText);
      });
    }
    else
    {
      //不檢查 Email 是否重複
      //如果 input 為空值得話把文字方塊變回藍色，提示文字變為空，把按鈕啟用
      $("#account").css({"border": "1px solid #00bfff"});
      $("#ex2").text("");
      $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
    }
  });//結束檢查


  //按下按鈕時檢查格式標籤輸入格式是否正確
  $("#save-user").on("click", function(){
    var fun = false, fac = false, fpw = false, fid = false, fst = false;
    var ver_English = /[A-Za-z]/g; //輸入英文驗證
    var ver_Number = /[0-9]/g; //輸入英文驗證
    var ver_Symbol = /[\\\.\/\*\-\+\~\`\!\_\@\#\$\%\^\&\(\)\{\}\[\]\:\;\"\'\<\>\,\=]/g; //輸入特殊符號驗證

    //username
    if ($("#username").val() == '') //驗證 username 是否為空白
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("未輸入暱稱").css({"color": "#ff0000"});
    }
    else if (ver_Symbol.test($("#username").val()))
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("不可輸入特殊符號").css({"color": "#ff0000"});
    }
    else if ($("#username").val().length <= 3) //驗證 username 字元長度是否少於 4 字元
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("匿稱輸入起碼超過 3 個字").css({"color": "#ff0000"});
    }
    else if ($("#username").val().length > 15)
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("匿稱輸入起碼超過字數限制字元").css({"color": "#ff0000"});
    }
    else
    {
      $("#username").css({"border": "1px solid #00bfff"});
      $("#ex1").text("");
      fun = true;
    }

    //account
    if ($("#account").val() == '') //驗證 account 是否為空白
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("未輸入帳號!").css({"color": "#ff0000"});
    }
    else if ($("#account").val().length < 8) //驗證 account 是否符合格式輸入
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號輸入起碼大於 8 個字元").css({"color": "#ff0000"});
    }
    else if (!ver_English.test($("#account").val()) && !ver_Number.test($("#account").val())) //驗證 account 輸入格式
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號必須是英文與數字組合!").css({"color": "#ff0000"});
    }
    else if (ver_Symbol.test($("#account").val()))
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("不可以輸入特殊符號").css({"color": "#ff0000"});
    }
    else if ($("#account").val().length > 20)
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號輸入超出字數限制字元").css({"color": "#ff0000"});
    }
    else
    {
      $("#account").css({"border": "1px solid #00bfff"});
      $("#ex2").text("");
      fac = true;
    }

    //passoword
    if ($("#password").val() == '') //驗證 password 是否為空白
    {
      $("#password").css({"border": "1px solid #ff0000"});
      $("#ex3").text("未輸入密碼!").css({"color": "#ff0000"});
    }
    else if ($("#password").val().length <= 8) //驗證 password 是否少於 8 字元
    {
      $("#password").css({"border": "1px solid #ff0000"});
      $("#ex3").text("密碼輸入起碼大於 8 個字元").css({"color": "#ff0000"});
    }
    else if(!ver_English.test($("#password").val()) && !ver_Number.test($("#password").val())) //驗證 password 輸入格式
    {
      $("#password").css({"border": "1px solid #ff0000"});
      $("#ex3").text("密碼必須是英文數字組合").css({"color": "#ff0000"});
    }
    else if (ver_Symbol.test($("#password").val()))
    {
      $("#password").css({"border": "1px solid #ff0000"});
      $("#ex3").text("不可以輸入特殊符號").css({"color": "#ff0000"});
    }
    else if ($("#password").val().length > 20)
    {
      $("#password").css({"border": "1px solid #ff0000"});
      $("#ex3").text("密碼輸入超出字元範圍").css({"color": "#ff0000"});
    }
    else
    {
      $("#password").css({"border": "1px solid #00bfff"});
      $("#ex3").text("");
      fpw = true;
    }

    if (typeof($("input[name=identity]:checked").val()) === "undefined")
    {
      $("#ex4").text("請選擇身分角色").css({"color": "#ff0000"});
    }
    else
    {
      $("#ex4").text("");
      fid = true;
    }

    if (typeof($("input[name=state]:checked").val()) === "undefined")
    {
      $("#ex5").text("請選擇狀態").css({"color": "#ff0000"});
    }
    else
    {
      $("#ex5").text("");
      fst = true;
    }

    //確認以上條件是否正確
    if (fun == true && fac == true && fpw == true && fid == true && fst == true) //驗證 password 與 password_confirm 兩者輸入是否一樣
    {
      //若以上條件都正確，則使用 ajax 傳送資料
      $.ajax({
        type: "POST", //使用 POST 方式傳送資料
        url: "php/addmember.php",
        data: {
          ad_un: $("#username").val(),
          ad_ac: $("#account").val(),
          ad_pw: $("#password").val(),
          ad_id: $("input[name=identity]:checked").val(),
          ad_st: $("input[name=state]:checked").val()
        },
        dataType: 'html'
      }).done(function(data){
        //傳送成功的時候
        //console.log(data);
        if (data == "yes")
        {
          alert("新增會員成功!");
          location.assign(location)
        }
        else
        {
          alert("新增會員失敗!");
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //傳送失敗的時候
        alert("有錯誤產稱請看 consle.log");
        console.log(jqXHR.resoneText);
      });
    }

    return false;
  });

});
