<?php
  require_once 'php/db.php';
  require_once 'php/functions.php';

  //取得章節所有資料
  $dates = get_all_chapter();

  if (!isset($_SESSION['is_login']) && !$_SESSION['is_login'])
  {
    header("Location: login.php");
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/del_chapter.js"></script>
  </head>
  <body>
    <div id="uname">
      <span class="username"><b><?php echo $_SESSION['is_username'];?></b></span>
      <span class="Mpasswd"><a href="modify_user.php"><b>修改密碼</b></a></span>
      <span class="loginOut"><a href="php/loginOut.php"><b>登出</b></a></span>
    </div>

    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span>
          <span class="bottom"><b>後台</b></span>
        </span>
      </span>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="container1">
      <!-- contents -->
      <div class="con-scope">
        <!-- 功能選項 -->
        <div class="box-tool">
          <div class="con-ac">
            <div class="navbar">
              <div class="dropdown">
                <a href="chapteradd.php">新增文章</a>
              </div>
            </div>
          </div>
        </div>

        <!-- contents -->
        <div class="user-content">
          <div class="content">
            <table>
              <tr class="box-black">
                <td></td>
                <td>章節</td>
                <td>內容</td>
                <td>發佈狀況</td>
                <td>上傳時間</td>
                <td>管理動作</td>
              </tr>
              <?php if(!empty($dates)):?>
                <?php foreach($dates as $key=>$row):?>
                  <?php
                    //將資料庫的內容儲存到 $content 變數中
                    $content = $row['content'];
                    
                    //去除標籤或特殊字元
                    $content = filter_var($content, FILTER_SANITIZE_STRING);
                    
                    //取得30的文字
                    $content = mb_substr($content, 0, 30, "UTF-8");
                  ?>

                  <tr class="box-gray" <?php echo ($key % 2 != 0)?"style='background-color:#d9d9d9'":"style='background-color:#ffffff'"?>>
                    <th class="usernumber"><?php echo $key + 1;?></th>
                    <th><?php echo $row['title'];?></th>
                    <th><?php echo $content;?></th>
                    <th><?php echo ($row['publish'] == '1')?"發佈":"不發佈"; ?></th>
                    <th><?php echo $row['addDate'];?></th>
                    <th>
                      <a class="edit" href='chapteredit.php?cha=<?php echo $row['id'];?>'>編輯</a>
                      <a class="delete" href="javascript:void(0);" data-id="<?php echo $row ['id'];?>">刪除</a>
                    </th>
                  </tr>
                <?php endforeach;?>
              <?php else:?>
                <tr>
                  <th colspan="6">無資料</th>
                </tr>
              <?php endif;?>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>
